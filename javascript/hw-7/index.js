'use strict';

let box = ["Ariel", 10, "Tide", "15", "Gillette", 20, false, 25, true, "Fairy", "Rexona", 30, "", "", undefined, null, {
    name: "Ivan",
    firstName: "Petrov"
}];

function showList(data) {

    let str = data.map(function (strItem) {
        return strItem;
    });

    let ul = document.createElement('ul');

    for (let i = 0; i < str.length; i++) {

        let unitList = document.createElement("li");
        unitList.innerHTML = str[i];
        ul.appendChild(unitList);

        document.body.appendChild(ul);
    }
}

showList(box);
