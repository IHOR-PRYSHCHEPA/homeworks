'use strict';

function createNewUser() {
    let newUser = {
        firstName: prompt("Введите ваше имя", ""),
        lastName: prompt("Введите вашу фамилию", ""),
        userBirthday: prompt("Введите день вашего рождения в формате", "dd.mm.yyyy"),

        getLogin: function () {
            return (this.firstName[0].toLowerCase() + this.lastName.toLowerCase())
        },

        getAge: function () {
            let birthday = new Date(this.userBirthday.slice(6), this.userBirthday.slice(3, 5) - 1, this.userBirthday.slice(0, 2));
            let currentDate = new Date;

            let userAge = currentDate.getFullYear() - birthday.getFullYear();
            if (currentDate.getMonth() < birthday.getMonth() && currentDate.getDate() > birthday.getDate()) {
                return (userAge - 1);
            } else if (currentDate.getMonth() <= birthday.getMonth() && currentDate.getDate() < birthday.getDate()) {
                return (userAge - 1);
            } else {
                return (userAge);
            }
        },

        getPassword: function () {
            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.userBirthday.slice(6))
        }
    };
    return newUser;
}

let newUser = createNewUser();
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());

