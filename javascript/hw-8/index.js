'use strict';

let form = document.querySelector('.card');
let temp = 0;
let tempCorrect = 0;
let input = document.createElement('input');
let enterCorrectPrice = document.createElement('span');
input.className = "price-card";
input.setAttribute('placeholder', 'price');

form.append(input);


input.onfocus = function () {
    this.style.borderColor = "green";
};
input.onblur = function () {
    enterCorrectPrice.remove();
    tempCorrect = 0;
    this.style.borderColor = "inherit";
    let span = document.createElement('span');
    span.className = "price-value";
    if (input.value > 0) {

        let button = document.createElement('button');
        if (temp == 0) {

            span.innerHTML = "Текущая цена: " + input.value;
            input.before(span);
            button.innerText = "X";
            button.style.width = "20px";
            button.style.marginLeft = "10px"
            button.type = "reset";
            input.before(button);
            temp = 1;
        }

        button.addEventListener('click', (e) => {
            span.remove();
            button.remove();

            console.log("i");
            input.value = "";
            temp = 0;

        });
    } else if (tempCorrect == 0) {
        this.style.borderColor = "red";

        enterCorrectPrice.innerText = 'Please enter correct price';
        input.after(enterCorrectPrice);
        tempCorrect = 1;
    }

}


