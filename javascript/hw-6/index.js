'use strict';

let box = ["Ariel", 10, "Tide", "15", "Gillette", 20, false, 25, true, "Fairy", "Rexona", 30, "", "", undefined, null, {name: "Ivan", firstName: "Petrov"}];

function filterBy(data, type) {
    let newArr = [];
    data.filter(function (item) {
        if (typeof (item) != type)
            newArr.push(item);
    });
    return newArr;
}

console.log(filterBy(box, "boolean"));
